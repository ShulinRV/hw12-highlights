function backlight() {
  const btns = document.querySelectorAll(".btn");

  document.addEventListener("keydown", function (event) {
    console.log(event.code);
    console.log(event.key);
    let pressKey = event.key.toLocaleUpperCase();
    btns.forEach((item) => {
      let itemUt = item.textContent.toLocaleUpperCase();
      if (pressKey === itemUt) {
        item.style.backgroundColor = "blue";
      } else {
        item.style.backgroundColor = "";
      }
    });
  });
}

backlight();
